package demo;

import jade.core.Agent;

public class HelloAgent extends Agent {
	@Override
	protected void setup() {
		System.out.println("Hello, I am " + getLocalName());
	}
}
