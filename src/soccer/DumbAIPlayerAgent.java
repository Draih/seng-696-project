package soccer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class DumbAIPlayerAgent extends Agent {
	float _PositionX;
	float _PositionY;
	float _TargetX;
	float _TargetY;
	String color;
	
	protected void setup() {
		Object[] args = getArguments();
		if (args.length != 3) {
			System.out.println("Must supply 3 arguments to an AI player: <xPos, yPos, Color>.");
			this.doDelete();
		}
		
		_PositionX = Integer.parseInt(args[0].toString());
		_PositionY = Integer.parseInt(args[1].toString());
		_TargetX = -1;
		_TargetY = -1;
		color = args[2].toString();
		
		addBehaviour(new TickerBehaviour(this, 30) {
			@Override
			protected void onTick() {
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("SoccerBall");
				dfd.addServices(sd);
				
				DFAgentDescription[] soccerBalls = null;
				try {
					soccerBalls = DFService.search(this.getAgent(), dfd);
				} catch (FIPAException fe) {
					fe.printStackTrace();
				}

				if (soccerBalls != null && soccerBalls.length > 0) {
					ACLMessage msgget = new ACLMessage(ACLMessage.INFORM);
					msgget.setContent("get");
					for (int i = 0; i < soccerBalls.length; i++)
						msgget.addReceiver(soccerBalls[i].getName());
					send(msgget);
				}

				ACLMessage msg1 = receive();
				while (msg1 != null) {
					String data[] = msg1.getContent().split(",");
					
					if (data[0].equals("update")) {
						float x = Float.parseFloat(data[2]);
						float y = Float.parseFloat(data[3]);
						
						if (_TargetX == -1 || _TargetY == -1 || (Math.pow(_PositionX - x, 2) + Math.pow(_PositionY - y, 2) < (Math.pow(_PositionX - _TargetX, 2) + Math.pow(_PositionY - _TargetY, 2)))) {
							_TargetX = x;
							_TargetY = y;
						}
					}

					msg1 = receive();
				}

				if (_TargetX != -1 && _TargetY != -1)
				{
					float difX = _TargetX - _PositionX;
					float difY = _TargetY - _PositionY;
					float length = (float)(Math.pow(difX, 2) + Math.pow(difY, 2));
					length = (float)(Math.pow(length, 0.5));
					
					if (length > 2) {
						_PositionX += difX / length;
						_PositionY += difY / length;
					}
					else {
						_TargetX = -1;
						_TargetY = -1;
					}
				}
				
				if (soccerBalls != null && soccerBalls.length > 0) {
					ACLMessage msg3 = new ACLMessage(ACLMessage.INFORM);
					msg3.setContent("kick," + (int)_PositionX + "," + (int)_PositionY);
					for (int i = 0; i < soccerBalls.length; i++)
						msg3.addReceiver(soccerBalls[i].getName());
					send(msg3);
				}
				
				ACLMessage msg4 = new ACLMessage(ACLMessage.INFORM);
				msg4.setContent("update," + getName() + "," + _PositionX + "," + _PositionY + "," + color);
				msg4.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
				send(msg4);
			}
		});
	}
	
	private int quickCount(boolean... vars) {
		int count = 0;
		for (boolean var : vars) {
			count += (var ? 1 : 0);
		}
		return count;
	}
	
	protected void takeDown() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent("delete," + getName());
		msg.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
		send(msg);
	}
}
