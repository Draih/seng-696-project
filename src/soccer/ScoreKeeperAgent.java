package soccer;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class ScoreKeeperAgent extends Agent {
	private Map<String, Integer> scores = new HashMap<String, Integer>();
	
	@Override
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("ScoreKeeper");
		sd.setName(getLocalName() + "-ScoreKeeper");
		dfd.addServices(sd);
		try {
			DFService.register(this,  dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		addBehaviour(new TickerBehaviour(this, 100) {
			@Override
			protected void onTick() {
				ACLMessage msg1 = receive();
				while (msg1 != null) {
					String data[] = msg1.getContent().split(",");
					
					if (data[0].equals("scored")) {
						String color = data[1];
						
						if (scores.containsKey(color)) {
							scores.put(color, scores.get(color) + 1);
						}
						else {
							scores.put(color, 1);
						}
						
						System.out.print("Scores");
						Iterator<Entry<String, Integer>> it = scores.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry<String, Integer> pair = it.next();
							System.out.print("," + pair.getKey() + "=" + pair.getValue());
						}
						System.out.println();
					}

					msg1 = receive();
				}
			}
		});
	}
}
