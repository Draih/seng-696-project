package soccer;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardState implements KeyListener {
    public boolean[] pressed;
    
    public KeyboardState() {
    	pressed = new boolean[222];
    }
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		pressed[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		pressed[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
