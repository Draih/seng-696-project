package soccer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class GoalAgent extends Agent {
	int _PositionX;
	int _PositionY;
	int _Width;
	int _Height;
	String color;
	
	protected void setup() {
		Object[] args = getArguments();
		if (args.length != 5) {
			System.out.println("Must supply 5 arguments to a goal: <xPos, yPos, Width, Weight, Color>");
			this.doDelete();
		}

		_PositionX = Integer.parseInt(args[0].toString());
		_PositionY = Integer.parseInt(args[1].toString());
		_Width = Integer.parseInt(args[2].toString());
		_Height = Integer.parseInt(args[3].toString());
		color = args[4].toString();
		
		int someoneScored = 0;

		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent("update," + getName() + "," + _PositionX + "," + _PositionY + "," + _Width + "," + _Height + "," + color);
		msg.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
		send(msg);
		
		addBehaviour(new TickerBehaviour(this, 100) {
			@Override
			protected void onTick() {
				// TODO Auto-generated method stub
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("SoccerBall");
				dfd.addServices(sd);
				
				DFAgentDescription[] soccerBalls = null;
				try {
					soccerBalls = DFService.search(this.getAgent(), dfd);
				} catch (FIPAException fe) {
					fe.printStackTrace();
				}
				
				if (soccerBalls != null && soccerBalls.length > 0) {
					ACLMessage msgget = new ACLMessage(ACLMessage.INFORM);
					msgget.setContent("get");
					for (int i = 0; i < soccerBalls.length; i++)
						msgget.addReceiver(soccerBalls[i].getName());
					send(msgget);
				}
				
				ACLMessage msg1 = receive();
				while (msg1 != null) {
					String data[] = msg1.getContent().split(",");
					
					if (data[0].equals("update")) {
						float x = Float.parseFloat(data[2]);
						float y = Float.parseFloat(data[3]);
						
						if (someoneScored <= 0 && x > _PositionX && x < _PositionX + _Width && y > _PositionY && y < _PositionY + _Height) {
							ACLMessage msg2 = new ACLMessage(ACLMessage.INFORM);
							msg2.setContent("reset");
							msg2.addReceiver(msg1.getSender());
							send(msg2);
							
							DFAgentDescription dfd2 = new DFAgentDescription();
							ServiceDescription sd2 = new ServiceDescription();
							sd2.setType("ScoreKeeper");
							dfd2.addServices(sd2);
							
							DFAgentDescription[] scoreKeepers = null;
							try {
								scoreKeepers = DFService.search(this.getAgent(), dfd2);
							} catch (FIPAException fe) {
								fe.printStackTrace();
							}
							
							if (scoreKeepers != null && scoreKeepers.length > 0) {
								ACLMessage msgscore = new ACLMessage(ACLMessage.INFORM);
								msgscore.setContent("scored," + color);
								for (int i = 0; i < scoreKeepers.length; i++)
									msgscore.addReceiver(scoreKeepers[i].getName());
								send(msgscore);
							}
						}
					}
					
					msg1 = receive();
				}
			}
		});
	}
	protected void takeDown() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent("delete," + getName());
		msg.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
		send(msg);
	}
}
