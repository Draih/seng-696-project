package soccer;

import java.awt.Color;

public class FieldEntity
{
	private String _Name;
	public String getName() { return _Name; }
	
	private float _XPosition;
	public float getXPosition() { return _XPosition; }
	public void setXPosition(float value) { _XPosition = value; }
	
	private float _YPosition;
	public float getYPosition() { return _YPosition; }
	public void setYPosition(float value) { _YPosition = value; }
	
	private String _Shape = "Circle";
	public String getShape() { return _Shape; }
	
	private int _Width;
	public int getWidth() { return _Width; }
	private int _Height;
	public int getHeight() { return _Height; }
	
	private Color _Color;
	public Color getColor() { return _Color; }
	
	public FieldEntity(String name, float xPos, float yPos, Color color)
	{
		_Name = name;
		_XPosition = xPos;
		_YPosition = yPos;
		_Color = color;
	}
	public FieldEntity(String name, float xPos, float yPos, int width, int height, Color color)
	{
		_Name = name;
		_XPosition = xPos;
		_YPosition = yPos;
		_Width = width;
		_Height = height;
		_Shape = "Rectangle";
		_Color = color;
	}
	public void update(float xPos, float yPos)
	{
		_XPosition = xPos;
		_YPosition = yPos;
	}
}
