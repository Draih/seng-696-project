package soccer;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public class FieldAgent extends Agent {
	private Frame frame;
	private Map<String, FieldEntity> entities = new HashMap<String, FieldEntity>();
	KeyboardState ks;
	
	@Override
	protected void setup() {
		frame = new Frame("Soccer Field Agent");
		frame.setSize(800, 600);
		frame.setVisible(true);
		
		JPanel jPanel = new JPanel(true) {
			@Override
			public void paint(Graphics g) {
				Graphics2D graphics = (Graphics2D) g;
				graphics.clearRect(0, 0, frame.getWidth(), frame.getHeight());
				for (String key : entities.keySet()) {
					FieldEntity e = entities.get(key);
					if (e.getShape().equals("Circle")) {
						graphics.setColor(e.getColor());
						graphics.fill(new Ellipse2D.Float(e.getXPosition() - 10, e.getYPosition() - 10, 20, 20));
						graphics.setColor(Color.black);
						graphics.draw(new Ellipse2D.Float(e.getXPosition() - 10, e.getYPosition() - 10, 20, 20));
					}
					else if (e.getShape().equals("Rectangle")) {
						Color c = e.getColor();
						Color alphaC = new Color(c.getRed(), c.getGreen(), c.getBlue(), 110);
						graphics.setColor(alphaC);
						graphics.fillRect((int)e.getXPosition(), (int)e.getYPosition(), e.getWidth(), e.getHeight());
					}
				}
			}
		};
		frame.add(jPanel);
		ks = new KeyboardState();
		frame.addKeyListener(ks);
		
		addBehaviour(new CyclicBehaviour(this) {
			@Override
			public void action() {
				ACLMessage msg = receive();
				if (msg != null) {
					String data[] = msg.getContent().split(",");
					
					if (data[0].equals("update")) {
						if (data.length == 5) {
							float x = Float.parseFloat(data[2]);
							float y = Float.parseFloat(data[3]);
							
							if (entities.containsKey(data[1]))
								entities.get(data[1]).update(x, y);
							else {
								Color c = null;
								try {
									Field field = Class.forName("java.awt.Color").getField(data[4]);
									c = (Color)field.get(null);
								}
								catch (Exception e) {
									c = Color.black;
								}
								entities.put(data[1], new FieldEntity(data[1], x, y, c));
							}
						}
						else if (data.length == 7) {
							float x = Float.parseFloat(data[2]);
							float y = Float.parseFloat(data[3]);
							int w = Integer.parseInt(data[4]);
							int h = Integer.parseInt(data[5]);
							
							if (entities.containsKey(data[1]))
								entities.get(data[1]).update(x, y);
							else {
								Color c = null;
								try {
									Field field = Class.forName("java.awt.Color").getField(data[6]);
									c = (Color)field.get(null);
								}
								catch (Exception e) {
									c = Color.black;
								}
								entities.put(data[1], new FieldEntity(data[1], x, y, w, h, c));
							}
						}
					}
					else if (data[0].equals("delete")) {
						entities.remove(data[1]);
					}
					else if (data[0].equals("keystate?")) {
						ACLMessage response = new ACLMessage(ACLMessage.INFORM);
						response.setContent("keys"
								+ "," + ks.pressed[KeyEvent.getExtendedKeyCodeForChar(data[1].charAt(0))]
								+ "," + ks.pressed[KeyEvent.getExtendedKeyCodeForChar(data[2].charAt(0))]
								+ "," + ks.pressed[KeyEvent.getExtendedKeyCodeForChar(data[3].charAt(0))]
								+ "," + ks.pressed[KeyEvent.getExtendedKeyCodeForChar(data[4].charAt(0))]);
						response.addReceiver(msg.getSender());
						send(response);
					}
				}
			}
		});
		
		addBehaviour(new TickerBehaviour(this, 25) {
			@Override
			protected void onTick() {
				jPanel.repaint();
			}
		});
	}
	
	@Override
	protected void takeDown() {
		if (frame.isEnabled())
			frame.dispose();
	}
}
