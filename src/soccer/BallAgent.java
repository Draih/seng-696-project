package soccer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class BallAgent extends Agent {
	float _PositionX;
	float _PositionY;
	float _OriginalX;
	float _OriginalY;
	String color;
	
	float _VelocityX;
	float _VelocityY;
	float _KickAmount;
	
	protected void setup() {
		Object[] args = getArguments();
		if (args.length != 3) {
			System.out.println("Must supply 3 arguments to a soccer ball: <xPos, yPos, Color>. Strongly recommend using \"white\" as its color.");
			this.doDelete();
		}
		
		_PositionX = Float.parseFloat(args[0].toString());
		_PositionY = Float.parseFloat(args[1].toString());
		_OriginalX = _PositionX;
		_OriginalY = _PositionY;
		color = args[2].toString();
		_VelocityX = 0;
		_VelocityY = 0;
		_KickAmount = 0;
		
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("SoccerBall");
		sd.setName(getLocalName() + "-SoccerBall");
		dfd.addServices(sd);
		try {
			DFService.register(this,  dfd);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		addBehaviour(new TickerBehaviour(this, 30) {
			@Override
			protected void onTick() {

				ACLMessage msg1 = receive();
				while (msg1 != null) {
					String data[] = msg1.getContent().split(",");
					
					if (data[0].equals("kick")) {
						int _XFrom = Integer.parseInt(data[1]);
						int _YFrom = Integer.parseInt(data[2]);
						
						if ((Math.pow(_PositionX - _XFrom, 2) + Math.pow(_PositionY - _YFrom, 2)) < 361) {
							_VelocityX = _PositionX - _XFrom;
							_VelocityY = _PositionY - _YFrom;
							float Length = (float)Math.pow(Math.pow(_VelocityX, 2) + Math.pow(_VelocityY, 2), 0.5);
							if (Length > 1) {
								_VelocityX /= Length;
								_VelocityY /= Length;
							}
							_KickAmount = 13;
						}
					}
					else if (data[0].equals("get")) {
						ACLMessage msg2 = new ACLMessage(ACLMessage.INFORM);
						msg2.setContent("update," + getName() + "," + _PositionX + "," + _PositionY + "," + color);
						msg2.addReceiver(msg1.getSender());
						send(msg2);
					}
					else if (data[0].equals("reset")) {
						_PositionX = _OriginalX;
						_PositionY = _OriginalY;
					}
					msg1 = receive();
				}
				
				if (_KickAmount > 0) {
					_PositionX += _VelocityX * _KickAmount;
					_PositionY += _VelocityY * _KickAmount;
					_KickAmount--;
				}
				
				ACLMessage msg2 = new ACLMessage(ACLMessage.INFORM);
				msg2.setContent("update," + getName() + "," + _PositionX + "," + _PositionY + "," + color);
				msg2.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
				send(msg2);
			}
		});
	}
	
	protected void takeDown() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent("delete," + getName());
		msg.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
		send(msg);
		
		try {
			DFService.deregister(this);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
}
