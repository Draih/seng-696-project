package soccer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class HumanPlayerAgent extends Agent {
	int _PositionX;
	int _PositionY;
	String color;

	char upKey;
	char downKey;
	char leftKey;
	char rightKey;
	
	boolean up = false;
	boolean down = false;
	boolean left = false;
	boolean right = false;
	
	protected void setup() {
		Object[] args = getArguments();
		if (args.length != 7) {
			System.out.println("Must supply 7 arguments to a human player: <xPos, yPos, Color, upKey, downKey, leftKey, rightKey>.");
			this.doDelete();
		}
		
		_PositionX = Integer.parseInt(args[0].toString());
		_PositionY = Integer.parseInt(args[1].toString());
		color = args[2].toString();
		upKey = ((String)args[3]).charAt(0);
		downKey = ((String)args[4]).charAt(0);
		leftKey = ((String)args[5]).charAt(0);
		rightKey = ((String)args[6]).charAt(0);
		
		addBehaviour(new TickerBehaviour(this, 30) {
			@Override
			protected void onTick() {
				ACLMessage msg1 = new ACLMessage(ACLMessage.INFORM);
				msg1.setContent("keystate?," + upKey + "," + downKey + "," + leftKey + "," + rightKey);
				msg1.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
				send(msg1);
				
				ACLMessage msg2 = receive();
				if (msg2 != null) {
					String data[] = msg2.getContent().split(",");
					
					if (data[0].equals("keys")) {
						up = Boolean.valueOf(data[1]);
						down = Boolean.valueOf(data[2]);
						left = Boolean.valueOf(data[3]);
						right = Boolean.valueOf(data[4]);
					}
				}
				
				int speed = 4 - quickCount(up, down, left, right);
				
				if (up) {
					_PositionY -= speed;
				}
				if (down) {
					_PositionY += speed;
				}
				if (left) {
					_PositionX -= speed;
				}
				if (right) {
					_PositionX += speed;
				}
				
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("SoccerBall");
				dfd.addServices(sd);
				
				DFAgentDescription[] soccerBalls = null;
				try {
					soccerBalls = DFService.search(this.getAgent(), dfd);
				} catch (FIPAException fe) {
					fe.printStackTrace();
				}
				
				if (soccerBalls != null && soccerBalls.length > 0) {
					ACLMessage msg3 = new ACLMessage(ACLMessage.INFORM);
					msg3.setContent("kick," + _PositionX + "," + _PositionY);
					for (int i = 0; i < soccerBalls.length; i++)
						msg3.addReceiver(soccerBalls[i].getName());
					send(msg3);
				}
				
				ACLMessage msg4 = new ACLMessage(ACLMessage.INFORM);
				msg4.setContent("update," + getName() + "," + _PositionX + "," + _PositionY + "," + color);
				msg4.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
				send(msg4);
			}
		});
	}
	
	private int quickCount(boolean... vars) {
		int count = 0;
		for (boolean var : vars) {
			count += (var ? 1 : 0);
		}
		return count;
	}
	
	protected void takeDown() {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setContent("delete," + getName());
		msg.addReceiver(new AID("SoccerField", AID.ISLOCALNAME));
		send(msg);
	}
}
